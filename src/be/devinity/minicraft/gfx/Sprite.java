package be.devinity.minicraft.gfx;

public class Sprite
{
	public int x;
	
	public int y;
	
	public int width;
	
	public int height;

	private SpriteSheet sheet;
	
	public static final int MIRROR_X = 0x01;
	public static final int MIRROR_Y = 0x02;
	
	
	public Sprite( SpriteSheet sheet , int x , int y , int width , int height )
	{
		this.sheet = sheet;
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
	}
	
	public void draw( Screen screen , int x , int y ) {
		this.draw( screen , x , y , 0 );
	}
	
	public void draw( Screen screen , int x , int y , int bits )
	{
		int px , py;
		
		
		boolean mirrorX = (bits & Sprite.MIRROR_X ) > 0;
		boolean mirrorY = (bits & Sprite.MIRROR_Y ) > 0;
		
		for( int ix = 0 ; ix < this.width ; ix++ ) {
			px = ix;
			if( mirrorX ) px = this.width - ix - 1;
			
			for( int iy = 0 ; iy < this.height ; iy++ ) {
				py = iy;
				if( mirrorY ) py = this.height - iy - 1;
				
				int cc = sheet.pixels[ this.x + ix + ( this.y + iy ) * sheet.width ];
				
				//if( ix == 0 || iy == 0 || ix == this.width - 1 || iy == this.height - 1 ) cc = 0xff1493;
				
				if( cc != 0 ) {
					
					//cc = this._tryOverlay( screen.pixels[ px + py * screen.width ] , cc );
					screen.pixels[ x + px + ( py + y ) * screen.width ] = cc; 
				}
				//screen.pixels[ i + j * screen.width ] = ( 255 << 16 ) + ( 255 << 8 ) + (255 );
			}
		}
	}
	
	protected int _tryOverlay( int original_color , int new_color )
	{
		return ( ( (( new_color >> 16 & 255 ) + ( original_color >> 16 & 255) ) / 2 ) << 16 ) + // R
		        ( ( (( new_color >> 8 & 255 ) + ( original_color >> 8 & 255 ) ) / 2 ) << 8 ) +   // G
		        ( ( (( new_color >> 16 & 255 ) + ( original_color >> 16 & 255 ) ) / 2 ) );       // B
	}
	
}
