package be.devinity.minicraft.gfx;

import java.util.Random;

import be.devinity.minicraft.Game;

public class Screen
{
	protected int width;
	protected int height;
	
	public int[] pixels;
	
	public Screen( int width , int height )
	{
		this.width = width;
		this.height = height;
		
		this.pixels = new int[ width * height ];
	}

	/**
	 * @return the width
	 */
	public int getWidth()
	{
		return width;
	}

	/**
	 * @param width the width to set
	 */
	public void setWidth( int width )
	{
		this.width = width;
	}

	/**
	 * @return the height
	 */
	public int getHeight()
	{
		return height;
	}

	/**
	 * @param height the height to set
	 */
	public void setHeight( int height )
	{
		this.height = height;
	}
	
	public void drawFrame( SpriteSheet spritesheet , int x , int y , int width , int height )
	{


		spritesheet.FRAME_TOP_LEFT.draw( this , x , y );
		
		for( int ix = spritesheet.FRAME_TOP_LEFT.width ; ix + spritesheet.FRAME_TOP_LEFT.width < width ; ix += spritesheet.FRAME_TOP_LEFT.width ) {
			spritesheet.FRAME_TOP.draw( this , x + ix , y );
			spritesheet.FRAME_TOP.draw(  this , x + ix, y + height - spritesheet.FRAME_TOP.height , Sprite.MIRROR_Y );
		}
		
		spritesheet.FRAME_TOP_LEFT.draw( this , x + width - spritesheet.FRAME_TOP_LEFT.width, y , Sprite.MIRROR_X );
		
		
		for( int iy = spritesheet.FRAME_LEFT.height ; iy + spritesheet.FRAME_LEFT.height < height ; iy += spritesheet.FRAME_LEFT.height ) {
			spritesheet.FRAME_LEFT.draw(  this , x , y + iy );
			spritesheet.FRAME_LEFT.draw(  this , x + width - spritesheet.FRAME_LEFT.width , y + iy , Sprite.MIRROR_X );
		}
		
		spritesheet.FRAME_TOP_LEFT.draw( this , x , y + height - spritesheet.FRAME_TOP_LEFT.height , Sprite.MIRROR_Y);
		spritesheet.FRAME_TOP_LEFT.draw( this , x + width - spritesheet.FRAME_TOP_LEFT.height , y + height - spritesheet.FRAME_TOP_LEFT.height , Sprite.MIRROR_X | Sprite.MIRROR_Y);
		
		
		
		return;
	}
	
	public void drawRandomColorStatic( int noiselevel )
	{
		Random r = new Random();

		for (int y = 0; y < Game.HEIGHT; y++) {
			for (int x = 0; x < Game.WIDTH; x++) {
				// int cc = screen.pixels[x + y * screen.w];
				// if (cc < 255) pixels[x + y * WIDTH] = colors[cc];
				int cy = ( noiselevel > 0 ? r.nextInt( noiselevel ) : 0 ) + ( 255 - noiselevel ) / Game.HEIGHT * y;
				int cx = ( noiselevel > 0 ? r.nextInt( noiselevel ) : 0 ) + ( 255 - noiselevel ) / Game.WIDTH * x;
				;
				int cd = (int) ( ( Game.WIDTH * 1.0 / Game.HEIGHT ) * Math.abs( x - ( Game.WIDTH - y * ( Game.WIDTH * 1.0 / Game.HEIGHT ) ) ) );
				//cd += 10 - r.nextInt( 20 );
				cd = Math.abs(  cd  );
				
				this.pixels[x + y * Game.WIDTH] = ( ( cd << 16 ) + ( cy << 8 ) + ( cx ) );
				
//				if( x < 10 && y < 10 ) {
//					pixels[x + y * Game.WIDTH] = this.spritesheet.pixels[ x + y * this.spritesheet.width ];
//				}
				
				//if( x % 3 == 2 ) pixels[ x + y * Game.WIDTH] = 0;
				//if( y % 3 == 2 ) pixels[ x + y * Game.WIDTH] = 0;
				
			}
		}
	}
}
