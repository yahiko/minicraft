package be.devinity.minicraft.gfx;

import java.awt.image.BufferedImage;

public class SpriteSheet
{

	public int width;

	public int height;

	public int[] pixels;

	public Sprite FRAME_TOP_LEFT;

	public Sprite FRAME_TOP;

	public Sprite FRAME_LEFT;
	


	public SpriteSheet(BufferedImage image)
	{
		this.width = image.getWidth();
		this.height = image.getHeight();
		this.pixels = image.getRGB( 0, 0, this.width, this.height, null, 0, this.width );

		for (int i = 0; i < pixels.length; i++) {
			//this.pixels[i] = ( this.pixels[i] & 0xff ) / 64;
			this.pixels[i] = ( this.pixels[i] & 0xffffff );
		}
		
		this.createSprites();
	}
	
	protected void createSprites()
	{
		this.FRAME_TOP_LEFT = new Sprite( this , 0 , 104 , 8 , 8 );
		this.FRAME_TOP = new Sprite( this , 8 , 104 , 8 , 8 );
		this.FRAME_LEFT = new Sprite( this , 16 , 104 , 8 , 8 );
	}
}
