package be.devinity.minicraft;

import java.awt.BorderLayout;
import java.awt.Canvas;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferInt;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.JFrame;

import be.devinity.minicraft.gfx.Screen;
import be.devinity.minicraft.gfx.Sprite;
import be.devinity.minicraft.gfx.SpriteSheet;

public class Game extends Canvas implements Runnable
{
	private static final long serialVersionUID = 1L;

	public static final int HEIGHT = 120;
	public static final int WIDTH = 160;
	public static final int SCALE = 3;
	public static final String NAME = "Minicraft the game";

	private BufferedImage image = new BufferedImage( Game.WIDTH, Game.HEIGHT, BufferedImage.TYPE_INT_RGB );
	private int[] pixels = ( (DataBufferInt) image.getRaster().getDataBuffer() ).getData();
	private boolean running = false;

	private SpriteSheet spritesheet;
	private Screen screen;

	public void render()
	{
		//System.out.println( "render();" );
		BufferStrategy bs = this.getBufferStrategy();

		if (bs == null) {
			this.createBufferStrategy( 3 );
			this.requestFocus();
			return;
		}

		
		this.screen.drawRandomColorStatic( 0 );
		
		this.screen.drawFrame( this.spritesheet , 8 , 8 , 50 , 50 );
		

		
		for( int y = 0 ; y < this.screen.getHeight() ; y++ ) {
			for( int x = 0 ; x < this.screen.getWidth() ; x++ ) {
				pixels[ x + y * this.screen.getWidth() ] = screen.pixels[ x + y * this.screen.getWidth() ];
			}
		}
		
		
		
		Graphics g = bs.getDrawGraphics();

		g.fillRect( 0, 0, this.getWidth(), this.getHeight() );

		g.drawImage( this.image, 0, 0, this.getWidth(), this.getHeight(), null );
		g.dispose();

		bs.show();
	}

	@Override
	public void run()
	{
		this.init();

		while (running) {
			this.render();

			try {
				Thread.sleep( 50 );
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
	}

	public void start()
	{
		this.running = true;
		new Thread( this ).start();
	}

	public void stop()
	{
		this.running = false;
	}

	private void init()
	{
		// initialize game and resources

		try {
			this.screen = new Screen( Game.WIDTH , Game.HEIGHT );
			this.spritesheet = new SpriteSheet( ImageIO.read( Game.class.getResourceAsStream( "/icons.png" ) ) );
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/**
	 * @param args
	 */
	public static void main( String[] args )
	{
		Game game = new Game();

		game.setMinimumSize( new Dimension( WIDTH * SCALE, HEIGHT * SCALE ) );
		game.setMaximumSize( new Dimension( WIDTH * SCALE, HEIGHT * SCALE ) );
		game.setPreferredSize( new Dimension( WIDTH * SCALE, HEIGHT * SCALE ) );

		JFrame frame = new JFrame( Game.NAME );

		frame.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );
		frame.setLayout( new BorderLayout() );
		frame.add( game, BorderLayout.CENTER );
		frame.pack();

		frame.setResizable( false );
		frame.setLocationRelativeTo( null );
		frame.setVisible( true );

		game.start();
	}
}
